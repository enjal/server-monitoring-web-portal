<?php
require('lib/Pusher.php');
// Kickstart the framework
$f3=require('lib/base.php');



// Load configuration
$f3->config('config.ini');
$f3->config('routes.ini');

//Start a session here
new Session();

$f3->run();
