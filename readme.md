# Real Time Notification System
This is a simple prototype project to notify the admin of a slight changes in the database. AJAX is implemented with timely poll to update the notifications every 5 seconds.

## Built With

* [FatFree PHP framework](https://fatfreeframework.com) - The web framework used
* [BootStrap](http://getbootstrap.com/) - For webpages
* MySQL- Database

## Running the tests
 * Login to the application with credentials myuser@myuser.com / myuser.
 * Open up the database. Open the notification table. Change the status column from read to unread or unread to read. The unread message will be notified to the user through dashboard.

## Configuration files
* routes.ini files contains all the defined routing.
* config.ini contains the global variables. Please make database configuration according to your settings.

