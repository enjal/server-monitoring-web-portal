<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
   <!-- <link rel="icon" href="../../favicon.ico"> -->

    <title>elloH</title>

    <!-- Bootstrap core CSS -->
    <link href="app/assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="app/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="app/assets/css/dashboard.css" rel="stylesheet">
    
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="dashboard">Server Monitoring Portal</a>
        </div>

        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
           <li><a href="dashboard">Dashboard</a></li>
          

           <li>    
              <span class="glyphicon glyphicon-bell" data-toggle="dropdown"><span class="badge">5</span></span>
    <ul class="dropdown-menu">
      <li role="presentation"><a role="menuitem" href="#">HTML</a></li>
      <li role="presentation" class="divider"></li>
      <li role="presentation"><a role="menuitem" href="#">CSS</a></li>
      <li role="presentation" class="divider"></li>
      <li role="presentation"><a role="menuitem"  href="#">JavaScript</a></li>
      <li role="presentation" class="divider"></li>
      <li role="presentation"><a role="menuitem" href="#">About Us</a></li>    
    </ul>
 </li> 

          <li>
            <div class="dropdown">
              <a href="#">
                <span class="glyphicon glyphicon-bell"><span class="badge">5</span>
                 <div id="notification">uh lala lala uh lala</div></span>
              </a>
            </div>
          </li>  

            <li><a href="user/logout">Logout</a>
            </li>            
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div> 
      </div>
    </nav>  

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Dashboard <span class="sr-only">(current)</span></a></li>
            <li><a href="#">Link 1</a></li>
            <li><a href="#">Link 2</a></li>
            <li><a href="#">Link 3</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="">Link 4</a></li>
            <li><a href="">Link 5</a></li>
            <li><a href="">Link 6</a></li>
            <li><a href="">Link 7</a></li>
            <li><a href="">Link 8</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="">Link 9</a></li>
            <li><a href="">Link 10</a></li>
            <li><a href="">Link 11</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Dashboard</h1>

          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
          </div> 
         
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery.min.js"><\/script>')</script>
    <script src="app/assets/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="app/assets/js/holder.min.js"></script>

    <script>

    </script>



<script type="text/javascript">
var ajax_call = function() {
  //your jQuery ajax code
   $(document).ready(function() { 
   var hero = "hello";          
            $.ajax({
                type: "POST",
                url: "notification", 
                data : {'expense_type': hero},
                success: function(data)
                {
                    $('#notification').html(data);   
                    
                }
               
            });
    });
};
var interval = 1000 * 60 * (1/60);  //set interval to every 10 seconds
setInterval(ajax_call, interval);
</script>

</body>
</html>
