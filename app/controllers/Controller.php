<?php

/* This is a parent controller which will be extended by other controllers*/
class Controller {

	/*Variable Declaration starts here */
	protected $db;
	protected $f3;
	/*Variable Declaration ends here */


	/* Function to check if user is logged in. 
	   Otherwise redirect to login page */
	function beforeroute(){
		if($this->f3->get('SESSION.user')==null){
			$this->f3->reroute('/login');
			exit; 
		}			
	}

	/*Function to check after re-routing */
	function afterroute(){
		
	}

	/* Set the database so that they can be invoked from every controller. 
	   The variables devdb, devdbusername, devdbpassword are retrieved in config.ini*/
	function __construct(){
		$f3=Base::instance();
		$this->f3=$f3;

		$db=new DB\SQL(
           $f3->get('devdb'),
           $f3->get('devdbusername'),
           $f3->get('devdbpassword'),
           array( \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION )
        );
        
		$this->db=$db;
	}
}