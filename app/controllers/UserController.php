<?php
class UserController extends Controller{

	function render(){
		
		$template = new Template;
		echo $template->render('login.htm');
	}

	function beforeroute(){
		
	}

	function authenticate(){
	    $useremail = $this->f3->get('POST.email');
		$password = $this->f3->get('POST.password');

		$user = new User($this->db);
		$user->getByEmail($useremail);
		//echo $password;

		//Check if the user exists
		if($user->dry()){
			$this->f3->reroute('/');
		}

		if(password_verify($password, $user->password)){
			$this->f3->set('SESSION.user', $user->email);
			$this->f3->reroute('/dashboard');		
		}
		else{
			$this->f3->reroute('/');
		}

	}

	function logout(){
		$this->f3->clear('SESSION');
		$this->f3->reroute('/');
	}

}