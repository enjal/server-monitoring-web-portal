<?php

class Notification extends DB\SQL\Mapper{

	public function __construct(DB\SQL $db){
		parent::__construct($db,'notification'); /*define notification table here*/
	}	

	public function getById($id){
		$this->load(array('id=?',$id));
		return $this->query;
	}

	public function getByStatus($status){
		$this->load(array('status=?',$status));
		return $this->query;
	}	

}